import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactService } from './services/contact.service';
import { Contact } from 'src/app/models/contact';

@Component({
  selector: 'znb-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contactForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private contactServie: ContactService
    ) { }

  ngOnInit() {

    this.contactForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      contact_number: ['', [Validators.required, Validators.maxLength(10)]],
      interested_in: ['', Validators.required],
      message: ['', [Validators.required, Validators.maxLength(300)]],
      terms_and_conditions: [false, Validators.requiredTrue]
    })

  }

  sendInfo() {
    if (this.contactForm.invalid) {
      this.contactForm.markAllAsTouched()
      return;
    }
    this.contactServie.sendInfo(this.contactForm.value);
  }

}

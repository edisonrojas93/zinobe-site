import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';

@Component({
  selector: 'znb-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'Zinobe';
  loading: boolean = true;

  constructor(
    translate: TranslateService,
    private router: Router
  ) {
    translate.setDefaultLang('es');
    translate.use('es');
  }

  ngOnInit(): void {

    this.router.events.subscribe((event) => {
      switch (true) {
        case event instanceof NavigationStart:
          this.loading = true;
          document.body.style.overflow = 'hidden';
          break;

        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError:
          this.loading = false;
          document.body.style.overflow = 'auto';
          break;
        default:
          break;
      }
    });
  }
}

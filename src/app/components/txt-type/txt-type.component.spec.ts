import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TxtTypeComponent } from './txt-type.component';

describe('TxtTypeComponent', () => {
  let component: TxtTypeComponent;
  let fixture: ComponentFixture<TxtTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TxtTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TxtTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

export interface Contact {
    name: string,
    email: string,
    contact_number: number,
    interested_in: string,
    message: string
    terms_and_conditions: boolean
}